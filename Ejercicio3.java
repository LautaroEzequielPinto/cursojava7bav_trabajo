package modulo1;

public class Ejercicio3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Tecla de Escape \t\t\t\t\t Significado");
		System.out.println("----------------\t\t\t\t\t -----------");
		System.out.println();
		System.out.println("     \\n        \t\t\t\t\t nueva linea");
		System.out.println("     \\t        \t\t\t\t\t un tab de espacio");
		System.out.println("     \\\"       \t\t\t\t\t se utiliza para poner comillas dobles dentro del texto por ejemplo (\"belencita)");
		System.out.println("     \\\\       \t\t\t\t\t se utiliza para escribir la \\ dentro del texto, por ejemplo \\algo\\");
		System.out.println("     \\'        \t\t\t\t\t se utiliza para las comillas simples para escribir por ejemplo 'princesita'");
		
	}

}
