package ejerciciosIfyCiclos;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;

public class Ejercicio_3_IF {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private final Action action = new SwingAction();

	/**
	 * Launch the application.
	 */
	String Mes;
	private final Action action_1 = new SwingAction_1();
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio_3_IF window = new Ejercicio_3_IF();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio_3_IF() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 600, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblIngreseElMes = new JLabel("Ingrese el mes el cual quiere saber sus dias:");
		lblIngreseElMes.setHorizontalAlignment(SwingConstants.CENTER);
		lblIngreseElMes.setBounds(0, 0, 584, 20);
		frame.getContentPane().add(lblIngreseElMes);
		
		JLabel lblNombredelmes = new JLabel("Nombre del mes que desea saber los dias:");
		lblNombredelmes.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombredelmes.setBounds(0, 40, 584, 14);
		frame.getContentPane().add(lblNombredelmes);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(10, 140, 564, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnEjecutar = new JButton("Ejecutar");
		btnEjecutar.setAction(action);
		btnEjecutar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Mes = textField_1.getText();
				if (Mes.equals("abril")|| Mes.equals("junio")|| Mes.equals("septiembre")|| Mes.equals("noviembre")||Mes.equals("Abril")|| Mes.equals("Junio")|| Mes.equals("Septiembre")|| Mes.equals("Noviembre")|| Mes.equals("ABRIL")|| Mes.equals("JUNIO")|| Mes.equals("SEPTIEMBRE")|| Mes.equals("NOVIEMBRE")){
					textField.setText(String.valueOf("El mes de "+Mes+" tiene 30 dias"));
				}
				else{
					if (Mes.equals("febrero") || Mes.equals("Febrero") || Mes.equals("FEBRERO")){
						textField.setText(String.valueOf("El mes de "+Mes+" tiene 28 dias,al menos que sea bisiesto que tiene 29 dias"));
				}
					else {
						if (Mes.equals("enero")|| Mes.equals("marzo")|| Mes.equals("mayo")|| Mes.equals("julio")|| Mes.equals("agosto")|| Mes.equals("octubre")|| Mes.equals("diciembre") || Mes.equals("Enero")|| Mes.equals("Marzo")|| Mes.equals("Mayo")|| Mes.equals("Julio")|| Mes.equals("Agosto")|| Mes.equals("Octubre")|| Mes.equals("Diciembre")||Mes.equals("ENERO")|| Mes.equals("MARZO")|| Mes.equals("MAYO")|| Mes.equals("JULIO")|| Mes.equals("AGOSTO")|| Mes.equals("OCTUBRE")|| Mes.equals("DICIEMBRE")){
							textField.setText(String.valueOf("El mes de "+Mes+" tiene 31 dias"));
					}
						else{
							textField.setText(String.valueOf("El mes que ingreso no existe"));
						}
				}
			  }
			}
		});
		btnEjecutar.setBounds(10, 210, 89, 23);
		frame.getContentPane().add(btnEjecutar);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setAction(action_1);
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textField.setText(null);
				textField_1.setText(null);
			}
		});
		btnAceptar.setBounds(485, 210, 89, 23);
		frame.getContentPane().add(btnAceptar);
		
		JLabel lblDiasdelmes = new JLabel("Dias del mes que introdujo:");
		lblDiasdelmes.setHorizontalAlignment(SwingConstants.CENTER);
		lblDiasdelmes.setBounds(0, 115, 584, 14);
		frame.getContentPane().add(lblDiasdelmes);
		
		textField_1 = new JTextField();
		textField_1.setBounds(229, 65, 121, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
	}

	@SuppressWarnings("serial")
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "Ejecutar");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
	@SuppressWarnings("serial")
	private class SwingAction_1 extends AbstractAction {
		public SwingAction_1() {
			putValue(NAME, "Aceptar");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
}
