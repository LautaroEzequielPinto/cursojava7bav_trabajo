package pantallas;

import java.awt.EventQueue;
import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Color;



public class Ejercicio1 {

	private JFrame frame;
	private JTextField txtustedPuedeVisualizar;
	private JTextField txtRecomendacionesEnLautaroepintogmailcom;
	
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio1 window = new Ejercicio1();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio1() {
		initialize();
		
		
	
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
	
		frame = new JFrame();
		frame.setTitle("Ventana creada por Pinto, Lautaro");
		frame.setBounds(100, 100, 450, 300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("VERDADERO");
		chckbxNewCheckBox.setBackground(Color.WHITE);
		chckbxNewCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(chckbxNewCheckBox, BorderLayout.WEST);
		
		JCheckBox chckbxNewCheckBox_1 = new JCheckBox("FALSO");
		chckbxNewCheckBox_1.setBackground(Color.WHITE);
		chckbxNewCheckBox_1.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(chckbxNewCheckBox_1, BorderLayout.EAST);
		
		txtustedPuedeVisualizar = new JTextField();
		txtustedPuedeVisualizar.setBackground(Color.WHITE);
		txtustedPuedeVisualizar.setHorizontalAlignment(SwingConstants.CENTER);
		txtustedPuedeVisualizar.setText("\u00BFUSTED PUEDE VISUALIZAR ESTA VENTANA?");
		txtustedPuedeVisualizar.setEditable(false);
		frame.getContentPane().add(txtustedPuedeVisualizar, BorderLayout.NORTH);
		txtustedPuedeVisualizar.setColumns(10);
		
		txtRecomendacionesEnLautaroepintogmailcom = new JTextField();
		txtRecomendacionesEnLautaroepintogmailcom.setBackground(Color.WHITE);
		txtRecomendacionesEnLautaroepintogmailcom.setText("RECOMENDACIONES EN: lautaroepinto14@gmail.com");
		txtRecomendacionesEnLautaroepintogmailcom.setEditable(false);
		txtRecomendacionesEnLautaroepintogmailcom.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(txtRecomendacionesEnLautaroepintogmailcom, BorderLayout.SOUTH);
		txtRecomendacionesEnLautaroepintogmailcom.setColumns(10);
		frame.setVisible(true);
	
		
	}
}
